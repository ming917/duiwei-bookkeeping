# 对味记账

 - 目的：为了方便自己平时记账所开发。
 - 项目框架：uniapp + uview。
 - 描述：日常收入支出记账app，服务端使用云函数，持久层使用云数据库。
 - 仅使用于安卓app。



![登陆](./docs/login.png)


![记一笔](./docs/bookkeeping.png)


![统计](./docs/statistics.png)


![明细](./docs/detailed.png)


![我的](./docs/user.png)





## 感谢

[**uView**](https://www.uviewui.com)

[**uni-app**](https://uniapp.dcloud.io)



## 版权信息

MIT

